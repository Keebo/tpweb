﻿[TestMethod]
public void TestMaj()
{
    Assert.AreEqual(true, Program.DebutMaj("Commence par une majuscule"));
    Assert.AreEqual(false, Program.DebutMaj("ne commence pas par une majuscule"));
}

[TestMethod]
public void TestPoint()
{
    Assert.AreEqual(false, Program.FinPoint("Ne ce termine pas par une point,"));
    Assert.AssEqual(true, Program.FinPoint("Ce termine par un point."))
}
